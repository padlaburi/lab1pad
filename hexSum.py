#/usr/bin/python

import sys


def main():
    numbers = sys.argv[1:]
    decimal = [int(x, 16) for x in numbers]
    total = sum(decimal)
    hx = hex(total)
    print hex(ord(hx[-2].upper())) 
    print hex(ord(hx[-1].upper()))


if __name__ == '__main__':
    main()
